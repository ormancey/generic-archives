#!/bin/bash

if [ "$1" == "" ]; then
    echo "Usage: dumpEgroupArchive egroupname"
    echo "       Will dump all eml original messages into a folder groupname"
    echo "       Requires a valid Kerberos token for an account authorized to access the archive."
    exit
fi

GroupName=$1
echo "Dumping $GroupName"

# Create and enter folderr
mkdir -p $GroupName

# Get valid cookie
cern-get-sso-cookie -u https://XXX_APPLICATION_ID_XXX.web.cern.ch/XXX_APPLICATION_ID_XXX/data/$GroupName -o $GroupName/cookie.txt 

if [ $? -ne 0 ]; then
	echo "Error cern-get-sso-cookie. Invalid kerberos token or unauthorized."
	exit
fi

cd $GroupName

# Get the json mail array
curl -s --cookie cookie.txt --cookie-jar cookie.txt -O https://XXX_APPLICATION_ID_XXX.web.cern.ch/XXX_APPLICATION_ID_XXX/data/$GroupName/indexjson.json

# PArse the JSON and downoad all eml messages
originalmessage="OriginalMessage.eml"
mailcount=0

for k in $(jq -r '.[].url' indexjson.json); do
    nicecount=`printf "%05d" $mailcount`
    newurl=$(echo $k | sed "s/\.html/\.$originalmessage/")
    outputfile=$nicecount.`basename $newurl`
    curl -f -s --cookie cookie.txt --cookie-jar cookie.txt "$newurl" -o $outputfile 
    let mailcount++
done

# End cleanup
rm -f cookie.txt indexjson.json
cd ..
 
