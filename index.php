<?php

include("includes/usergroupmanager.php");

?>

<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="css/entrypage.css">
    <title>XXX_APPLICATION_NAME_XXX</title>

    <script src="js/ag-grid-community.min.js"></script>
    <!-- daterangepicker https://longbill.github.io/jquery-date-range-picker/ -->
    <link rel="stylesheet" href="css/daterangepicker.min.css">
    <script type="text/javascript" src="js/moment.min.js"></script>
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.daterangepicker.min.js"></script>
    <!-- https://github.com/devbridge/jQuery-Autocomplete -->
    <script type="text/javascript" src="js/jquery.autocomplete.min.js"></script>

    <?php
    // Get user groups filtered to only the existing in this XXX_APPLICATION_ID_XXX site
    $usergroups = getFilteredUserGroups();
    //echo '<br/>usergroups...' . var_dump($usergroups);
    
    // Build the javascript array of groupbrowser data
    // Load all array, then filter out only the groups with current user groups member of ACL field
    // $groupbrowserjson = file_get_contents('data/groupbrowser.json');
    // $groupbrowser = json_decode($groupbrowserjson);

    // $griddata = array_filter(
    //     $groupbrowser,
    //     function ($e) use ($usergroups) {
    //         foreach ($e->ACL as $oneacl) {
    //             if (in_array($oneacl, $usergroups))
    //                 return true;
    //         }
    //         return false;
    //     }
    // );


    echo '<script language="Javascript">';
    echo 'var rowData = ' . json_encode(array_values($usergroups)) . ';'; // The groups array

    echo '</script>';
    ?>

    <script src="js/topdata.php.js"></script>

</head>
<body>
    <!-- toolbar -->
    <?php include("includes/toolbar.html") ?>

    
    <div id="Title">XXX_APPLICATION_NAME_XXX</div>

    <div id="Home">

        <div>
            <div id="Search"  style="display: inline-block; vertical-align: top;">
                <!-- search form -->
                <?php include("includes/searchform.php") ?>
            </div>

            <?php include("includes/taskbar.php") ?>

        </div>

        <div id="GroupBrowser">
            <div><h3>Browse groups</h3></div>
            <div class="note">Click on the icons
<span class="ag-icon ag-icon-menu" style="display: inline; font-size: 14px;"></span>
                    on the headers below to filter results, and click headers to sort results

            </div>
                <div id="myGrid" style="height: 600px; width: 100%;" class="ag-theme-balham">
            </div>

            <?php
                if (isset($_SESSION['total_groups'])) {
                    echo '<div class="note">';
                    echo 'Displaying ' . count($usergroups) . ' groups you are authorized to access.';                   
                    echo ' Total ' . $_SESSION['total_groups'] . ' groups in the archive,';
                    echo ' for a total of ' . $_SESSION['total_mails'] . ' mails.';
                    echo '</div>';
                } 
            ?> 
</div>

    </div>

    <!-- footer -->
    <?php include("includes/footer.html") ?>

    <script type="text/javascript" src="js/searchformtools.js"></script>
    <?php
    // Start with hidden advanced search
    echo '<script>';
    if (empty($DateRange) && empty($GroupFilter))
        echo "$('#SearchAdvanced').hide();";
    else
        echo 'enableadvancedsearch();';
    echo '</script>';
    ?>

</body>
</html>