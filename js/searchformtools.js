
function enableadvancedsearch()
{
	$('#EnableSearchAdvanced').hide(); 
	$('#SearchAdvanced').show();
}


// Date picker
$('#date-range9').dateRangePicker(
	{
		startOfWeek: 'monday',
    	separator : ' TO ',
    	format: '"DD-MM-YYYY HH:mm"',
    	autoClose: false,
		time: {
			enabled: true
		},
	});

// Group filter autocomplete
// Suggestion array rebuild
var grpautocomplete = [];
if (rowData) {
	for (var i = 0; i < rowData.length; i++) {
		grpautocomplete.push({
			value: rowData[i].GroupName,
			data: rowData[i].GroupName
		});
	}
}

// Trigger autocomplete
$('#group-filter').autocomplete({
    lookup: grpautocomplete,
    // onSelect: function (suggestion) {
    //     alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
    // }
});