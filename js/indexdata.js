var columnDefs = [
    {headerName: "Subject", field: "Subject", 
        sortable: true, filter: true,
        cellRenderer: function(params) {
            return '<a href="' + params.data.url + '">'+ params.value +'</a>'
        }
    },
    {headerName: "From", field: "From", sortable: true, filter: true},
    {headerName: "Replies", field: "Replies", sortable: true, filter: true},
    {headerName: "Date", field: "Date", sortable: true, filter: true}
];
    
//   var rowData = [];      // The groups array
//       $.getJSON("indexjson.json", function (data) {
//           $.each(data, function (index, value) {
//               rowData.push(value);  
//           });

//           gridOptions.api.setRowData(gridOptions.rowData)
//       });

// let the grid know which columns and what data to use
var gridOptions = {
    columnDefs: columnDefs,
    pagination: true
//    ,    rowData: rowData
};

// setup the grid after the page has finished loading
document.addEventListener('DOMContentLoaded', function() {
    var gridDiv = document.querySelector('#myGrid');
    new agGrid.Grid(gridDiv, gridOptions);

     agGrid.simpleHttpRequest({url: 'indexjson.json'}).then(function(data) {
       gridOptions.api.setRowData(data);
   });

    gridOptions.api.sizeColumnsToFit();
});
    
