var columnDefs = [
    {headerName: "Group", field: "GroupName", 
        sortable: true, filter: true,
        cellRenderer: function(params) {
            return '<a href="data/' + params.value + '/index.html">'+ params.value +'</a>'
        }
    },
    {headerName: "Department", field: "Department", sortable: true, filter: true},
    {headerName: "Experiment", field: "Experiment", sortable: true, filter: true},
    {headerName: "Project", field: "Project", sortable: true, filter: true},
    {headerName: "Club", field: "Club", sortable: true, filter: true},
    {headerName: "Mails", field: "MailCount", sortable: true, filter: false}
];
    
//  var rowData = [];      // The groups array
//      $.getJSON("data/groupbrowser.json", function (data) {
//          $.each(data, function (index, value) {
//              rowData.push(value);  
//          });

//          gridOptions.api.setRowData(gridOptions.rowData)
//      });

// let the grid know which columns and what data to use
var gridOptions = {
    columnDefs: columnDefs,
    pagination: true
//    ,    rowData: rowData
};

// setup the grid after the page has finished loading
document.addEventListener('DOMContentLoaded', function() {
    var gridDiv = document.querySelector('#myGrid');
    new agGrid.Grid(gridDiv, gridOptions);

      agGrid.simpleHttpRequest({url: 'data/groupbrowser.json'}).then(function(data) {
        gridOptions.api.setRowData(data);
    });

    gridOptions.api.sizeColumnsToFit();
});
    
