<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="css/entrypage.css">
    <title>XXX_APPLICATION_NAME_XXX</title>
</head>
<body>
    <!-- toolbar -->
    <?php include("includes/toolbar.html") ?>

    <div id="Title">
        <div style="display: inline-block;">
            <div id="Static"><a href="https://XXX_APPLICATION_ID_XXX.web.cern.ch/XXX_APPLICATION_ID_XXX/">XXX_APPLICATION_NAME_XXX</a></div>
        </div>
        <div style="display: inline-block;">
            <div id="Sep"></div>
        </div>
        <div style="display: inline-block;">
            <div id="GroupName">What's new</div>
        </div>
    </div>

    <div id="Home">

        <h2>Welcome to the XXX_APPLICATION_NAME_XXX</h2>

        <h4>Overview</h4>
        <p>
        The XXX_APPLICATION_NAME_XXX service runs on simple and independant technologies, that will allow the archive to stay accessible 
        over the years without the need for upgrades or data migration.
        </p>

        <h4>Technical background</h4>
        The XXX_APPLICATION_NAME_XXX are using:
        <ul>
            <li>EOS Open Storage (<a href="http://eos.web.cern.ch/" target="_blank">http://eos.web.cern.ch/</a>) for data storage.</li>
            <li>CERN Web hosting (<a href="http://cern.ch/webservices" target="_blank">http://cern.ch/webservices</a>) for Web hosting</li>
            <li>CERN Search As a Service (<a href="https://cern-search.docs.cern.ch/" target="_blank">https://cern-search.docs.cern.ch/</a>) for Archives content search</li>
            <li>CERN OpenStack (<a href="https://clouddocs.web.cern.ch/" target="_blank">https://clouddocs.web.cern.ch/</a>) for the incoming mail handler</li>
        </ul>

        <p style="text-align: center; font-size: larger;">
        <a href="https://XXX_APPLICATION_ID_XXX.web.cern.ch/XXX_APPLICATION_ID_XXX/">[Continue to the XXX_APPLICATION_NAME_XXX]</a>
        </p>
    </div>

    <!-- footer -->
    <?php include("includes/footer.html") ?>

</body>
</html>