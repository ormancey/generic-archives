<form method="GET" action="search.php">
<div><h3>Search message archives</h3></div>
<div>
    <div style="display: inline-block;">
        <input name="InputKeywords" type="text" maxlength="200" 
            title="Search..." class="searchinput" alt="Search..." value="<?php echo htmlspecialchars($InputKeywords) ?>">
    </div>
    <div style="display: inline-block;">
        <input class="searchbutton" type="submit" name="search" value="Search" />
    </div>
</div>

<div id="EnableSearchAdvanced">
    <a href="javascript:enableadvancedsearch();">Advanced Search</a>
</div>
<div id="SearchAdvanced">
    <div style="margin-top: 10px; margin-bottom: 10px;">
        <div style="display: inline-block;" class="advname">Date range:</div>
        <div style="display: inline-block;"><input name="DateRange" id="date-range9" type="text" class="advinput" value="<?php echo htmlspecialchars($DateRange) ?>" /></div>
        <div style="display: inline-block;"><span class="clearfield" onclick="$('#date-range9').data('dateRangePicker').clear();">clear</span></div>
    </div>

    <div>
    <!-- https://www.devbridge.com/sourcery/components/jquery-autocomplete/
    https://github.com/devbridge/jQuery-Autocomplete -->
        <div style="display: inline-block;" class="advname">Group filter:</div> 
        <div style="display: inline-block;"><input name="GroupFilter" id="group-filter" type="text" class="advinput" value="<?php echo htmlspecialchars($GroupFilter) ?>" /></div>
        <div style="display: inline-block;"><span class="clearfield" onclick="$('#group-filter').val('');">clear</span></div>
    </div>
</div>

<!-- <div>
    <input type="radio" name="rdExperiment" value="All" checked="true">All archives</input>
    <input type="radio" name="rdExperiment" value="ALICE">ALICE</input>
    <input type="radio" name="rdExperiment" value="ATLAS">ATLAS</input>
    <input type="radio" name="rdExperiment" value="CMS">CMS</input>
    <input type="radio" name="rdExperiment" value="LHCB">LHCb</input>
</div> -->

</form>
