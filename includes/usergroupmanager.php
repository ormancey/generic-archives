<?php

// Tools
function has_prefix($string, $prefix) {
    return substr($string, 0, strlen($prefix)) == $prefix;
}

function getFilteredUserGroups() {

    // Start session stuff
    // DISABLED FOR TESTING AND DEBUG
    echo 'Warning: Sessions disabled for debugging';
    //session_start();
    
    $time = $_SERVER['REQUEST_TIME'];
    // 30 min session
    $timeout_duration = 1800;

    /**
    * Here we look for the user's LAST_ACTIVITY timestamp. If
    * it's set and indicates our $timeout_duration has passed,
    * blow away any previous $_SESSION data and start a new one.
    */
    if (isset($_SESSION['LAST_ACTIVITY']) && 
        ($time - $_SESSION['LAST_ACTIVITY']) > $timeout_duration) {
        session_unset();
        session_destroy();
        session_start();
    }

    /**
    * Finally, update LAST_ACTIVITY so that our timeout
    * is based on it and not the user's login time.
    */
    $_SESSION['LAST_ACTIVITY'] = $time;

    // uncomment to destroy session and debug 
    //session_destroy();

    // If list is in session skip recalculation
    if (isset($_SESSION['filtered_usergroups'])) {
        $usergroups = $_SESSION['filtered_usergroups'];
        return $usergroups;
    }

    // Get user groups, clean entries from CMF values to decrease size, and rebuild with commas
    $usergroupsStr = getenv('ADFS_GROUP');
    $usergroupsArray = explode(";", $usergroupsStr);
    // Array to return
    $usergroups = array();

    // Filter out only the groups available in XXX_APPLICATION_ID_XXX
    $groupbrowserjson = file_get_contents('data/groupbrowser.json');
    $groupbrowser = json_decode($groupbrowserjson);

    //echo '<br/>found ';
    //var_dump($groupbrowser);

    // foreach ($usergroupsArray as $groupvalue) {
    //     if (!has_prefix($groupvalue, 'CMF_'))
    //     {
    //         $neededObject = array_filter(
    //             $groupbrowser,
    //             function ($e) use ($groupvalue) {
    //                 return $e->GroupName == $groupvalue;
    //             }
    //         );
    //         if ($neededObject) {
    //             //var_dump($neededObject);
    //             array_push($usergroups, $groupvalue);
    //         }
    //     }
    // }

    // Load all array, then filter out only the groups with current user groups are member of ACL field
    $usergroups = array_filter(
        $groupbrowser,
        function ($e) use ($usergroupsArray) {
            foreach ($e->ACL as $oneacl) {
                if (in_array($oneacl, $usergroupsArray))
                    return true;
                if ($oneacl === 'authenticated users')
                    return true;
            }
            return false;
        }
    );

    // Calc total number of mails
    $sumMailCount = array_reduce($groupbrowser, function($carry, $item)
    {
        return $carry + $item->MailCount;
    });
//    var_dump($sumMailCount);

    // Save in session
    $_SESSION['filtered_usergroups'] = $usergroups;
    $_SESSION['total_groups'] = count($groupbrowser);
    $_SESSION['total_mails'] = $sumMailCount;

    return $usergroups;
}

function getFilteredUserGroupsAsStringWithCERNdomain($usergroups)
{
    $ret = array();
    foreach ($usergroups as $onegroup)
        array_push($ret, $onegroup->GroupName . '@cern.ch');

    return implode(',', $ret);
}


?>
