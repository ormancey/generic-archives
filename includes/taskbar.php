
<!--
<div id="GroupTasks" style="display: inline-block; vertical-align: top; margin-left: 50px;">
                <div><h3>Group management tasks</h3></div>
                <div id="Links">
                    <div style="display: inline-block; vertical-align: top">
                        <a href="https://e-groups.cern.ch/e-groups/EgroupsSearchOwner.do">Show all groups I own or manage</a>
                        <br/><a href="https://e-groups.cern.ch/e-groups/EgroupsSearchMember.do">Show all groups I am on</a>
                        <br/><a href="https://e-groups.cern.ch/e-groups/EgroupsNewStatic.do">Create new group (static)</a>
                        <br/><a href="https://e-groups.cern.ch/e-groups/EgroupsNewDynamic.do">Create new group (dynamic)</a>
                    </div>
                    <div style="display: inline-block; vertical-align: top">
                        <a href="https://e-groups.cern.ch/e-groups/EgroupsManageGroupsForMember.do">Manage groups for one person</a>
                        <br/><a href="https://e-groups.cern.ch/e-groups/EgroupsManageOwnerAdmin.do">Manage owner/admin</a>
                        <br/><a href="https://aisdocs.web.cern.ch/display/ED/E-groups+Documentation">E-groups documentation</a>
                        <br/><a href="http://cern.ch/e-groups-help">E-groups help, training & video tutorials</a>
                    </div>
                </div>
            </div>

            <div id="ArchiveTasks" style="display: inline-block; vertical-align: top; margin-left: 50px;">
                <div><h3>Archive tools</h3></div>
                <div id="Links">
                    <a href="scripts/dumpEgroupArchive.sh">Script to dump archive</a>
                    <br/>Shell script to run on a CERN managed linux machine. 
                    <br/>Dumps the raw contents of the specified archive in EML format.
                </div>
            </div>
-->