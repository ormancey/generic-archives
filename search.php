<?php

include("includes/usergroupmanager.php");

$PAGE_SIZE = '25';

$egroupsarchivesbaseurl = "https://XXX_APPLICATION_ID_XXX.web.cern.ch/XXX_APPLICATION_ID_XXX/data";

// Handle the supid magic_quotes that disappear after php5.4
if (get_magic_quotes_gpc()) {
    $InputKeywords = stripslashes($_GET['InputKeywords']);
    $DateRange = stripslashes($_GET["DateRange"]);
    $GroupFilter = stripslashes($_GET["GroupFilter"]);
    $rdExperiment = stripslashes($_GET["rdExperiment"]);
    $prevnext = stripslashes($_GET["prevnext"]);
} else {
    $InputKeywords = $_GET["InputKeywords"];
    $DateRange = $_GET["DateRange"];
    $GroupFilter = $_GET["GroupFilter"];
    $rdExperiment = $_GET["rdExperiment"];
    $prevnext = $_GET["prevnext"];
}

// Get user groups filtered to only the existing in this XXX_APPLICATION_ID_XXX site
$usergroups = getFilteredUserGroups();


if (($InputKeywords) || ($prevnext))
{
    // grparch token
    $authorization = "Authorization: Bearer XXX_SEARCH_TOKEN_XXX";
    $cernsearchapiurl = "https://XXX_SEARCH_ID_XXX.web.cern.ch/api/records/";

    $usergroupsAsString = getFilteredUserGroupsAsStringWithCERNdomain($usergroups);
    //echo '<br/>usergroupsAsString...' . var_dump($usergroupsAsString);

    if ($prevnext) {
        $searchparameters = array(
            'access' => $usergroupsAsString,
            'highlight' => '_data.*'
        );
        // if prevnext is not valid, ignore it all
        $prevnext = urldecode($prevnext);
        if (substr_compare($prevnext, $cernsearchapiurl, 0, strlen($cernsearchapiurl)) === 0)
        {
            $prevnextquerystring = substr($prevnext, strlen($cernsearchapiurl) + 1); // remove fqdn/?
            parse_str($prevnextquerystring, $prevnextparameters);
            if (get_magic_quotes_gpc())            
                $prevnextparameters['q'] = stripslashes($prevnextparameters['q']); //fix shitty escapes
            $InputKeywords = $prevnextparameters['q']; // To refill the form
            $querystring = http_build_query($searchparameters) . '&' . http_build_query($prevnextparameters);
        }
    } 
    else 
    {
        $tmpQuery = $InputKeywords;
        if ($DateRange) {
            // + instead of AND
            $tmpQuery = $tmpQuery . ' +date:{' . $DateRange . '}';
        }
        if ($GroupFilter) {
            // + instead of AND
            $tmpQuery = $tmpQuery . ' +group:' . $GroupFilter;
        }

        $searchparameters = array(
            'q' => $tmpQuery,
            'access' => $usergroupsAsString,
            'size' => $PAGE_SIZE,
            'highlight' => '_data.*'
        );
        $querystring = http_build_query($searchparameters);
    }

// debugging...
//echo '<br/>' . $querystring;

    $c = curl_init();

    curl_setopt($c, CURLOPT_URL, $cernsearchapiurl . '?' .  $querystring);
    // Inject the token into the header
    curl_setopt($c, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Accept: application/json', $authorization));
    /*On indique à curl de nous retourner le contenu de la requête plutôt que de l'afficher*/
    curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
    /*On indique à curl de ne pas retourner les headers http de la réponse dans la chaine de retour*/
    curl_setopt($c, CURLOPT_HEADER, false);

    $output = curl_exec($c);
    if($output === false)
    {
            //trigger_error('Error curl : '.curl_error($c),E_USER_WARNING);
            $error = 'Error curl : ' . curl_error($c);
    }
    else
    {
            //var_dump($output);
            $results = json_decode($output);
            //var_dump($results);
    }
    curl_close($c);

    if ($results->status === 500)
        $error = 'Error search: ' . $results->message;
}

?>

<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="css/entrypage.css">

    <!-- daterangepicker https://longbill.github.io/jquery-date-range-picker/ -->
    <link rel="stylesheet" href="css/daterangepicker.min.css">
    <script type="text/javascript" src="js/moment.min.js"></script>
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.daterangepicker.min.js"></script>
    <!-- https://github.com/devbridge/jQuery-Autocomplete -->
    <script type="text/javascript" src="js/jquery.autocomplete.min.js"></script>

    <?php
    echo '<script language="Javascript">';
    echo 'var rowData = ' . json_encode($usergroups) . ';'; // The groups array
    echo '</script>';
    ?>

    <title>XXX_APPLICATION_NAME_XXX Search</title>
</head>
<body>
    <!-- toolbar -->
    <?php include("includes/toolbar.html") ?>

    <div id="Title">
        <div style="display: inline-block;">
            <div id="Static"><a href="https://XXX_APPLICATION_ID_XXX.web.cern.ch/XXX_APPLICATION_ID_XXX/">XXX_APPLICATION_NAME_XXX</a></div>
        </div>
        <div style="display: inline-block;">
            <div id="Sep"></div>
        </div>
        <div style="display: inline-block;">
            <div id="GroupName">Search</div>
        </div>
    </div>

    <div id="SearchPage">
        <div>
            <div id="Search">
                <!-- search form -->
                <?php include("includes/searchform.php") ?>
            </div>
        </div>

        <div id="SearchResults">
            <?php
                if ($results) {

                    if ($results->hits->total)
                        echo '<div id="SearchCount">Found ' . $results->hits->total . ' results:</div>';

                    foreach ($results->hits->hits as $result)
                    {
                        //var_dump($result->metadata->_data);
                        echo '<div class="oneresult">';

                            echo '<div class="searchfrom">';
                            echo '<a href="' . $egroupsarchivesbaseurl . '/' . $result->metadata->group . '/' . $result->metadata->mailid . '.html">';

                            echo 'Group: ';
                            //echo '<a href="' . $egroupsarchivesbaseurl . '/' . $result->metadata->group . '/">';
                            echo $result->metadata->group;
                            //echo '</a>';
                            echo '<span class="searchsep"></span>';
                            echo 'From: ' . htmlentities($result->metadata->_data->from);

                            echo '</a>';
                            echo '</div>';

                            echo '<div class="searchsubject">';
                            // Missing mail id !
                            echo '<a href="' . $egroupsarchivesbaseurl . '/' . $result->metadata->group . '/' . $result->metadata->mailid . '.html">';
                            if ($result->highlight->{'_data.subject'}[0])
                                echo $result->highlight->{'_data.subject'}[0];
                            else
                                echo $result->metadata->_data->subject;
                            echo '</a>';
                            echo '</div>';

                            echo '<div class="searchbody">';
                            if ($result->metadata->date)
                                echo '<span class="searchdate">' . $result->metadata->date . ' - </span>';
                            if ($result->highlight->{'_data.body'}[0])
                                echo $result->highlight->{'_data.body'}[0];
                            else
                                echo substr($result->metadata->_data->body, 0, 250);
                            echo '...</div>';

                        echo '</div>';
                    }
                }
                if ($error)
                    echo $error;

                //var_dump($results);
            ?>
        </div>

        <div id="Pagination">
            <?php
                if ($results) {
                    echo '<div>';
                    if ($results->links->prev) {
                        $prevparameters = array(
                            'prevnext' => urlencode($results->links->prev)
                        );
                        echo '<a href="' . 'search.php?' . http_build_query($prevparameters) . '">&lt;&lt;&lt; Prev</a>';
                    } 
                    else
                        echo '&lt;&lt;&lt; Prev';

                    echo '&nbsp;&nbsp;&nbsp;';

                    if ($results->links->next) {
                        $nextparameters = array(
                            'prevnext' => urlencode($results->links->next)
                        );
                        echo '<a href="' . 'search.php?' . http_build_query($nextparameters) . '">Next &gt;&gt;&gt;</a>';
                    }
                    else
                        echo 'Next &gt;&gt;&gt;';

                    echo '</div>';
                }
            ?>
        </div>


    </div>

    <script type="text/javascript" src="js/searchformtools.js"></script>
    <?php
    // Start with hidden advanced search
    echo '<script>';
    if (empty($DateRange) && empty($GroupFilter))
        echo "$('#SearchAdvanced').hide();";
    else
        echo 'enableadvancedsearch();';
    echo '</script>';
    ?>

</body>
</html>
